tp1;

%-------------------------------------------------------------------------------
% Armado de ventana Dolph-Chebyshev

% Esta ventana se define mas facil en el dominio de la frecuencia, por lo tanto
%  primero armamos su respuesta en frecuencia y luego anti-transformamos
%  Fourier para obtener la ventana en el dominio del tiempo.

L_dol = 203;  % (IMPAR!) longitud de ventana
alpha = -60; % nivel [dB] (negativo) de maximo lobulo secundario (equiripple en este caso)
             % *** NOTA: Comentar este tema

% armamos coeficiente $ \beta $
beta = cosh( (L_dol-1)^(-1) * acosh( 10^(-alpha/20) ) ); %

% armamos factor: $ \beta * cos(\omega / 2) $
% , evaluado en: $ \omega =  2 \pi k / L_dol ;  0 \leq k \leq (L_dol-1) $
beta_cos = beta * cos( (pi/L_dol) * (0:L_dol-1) );

% buscamos indices segun condicion en $ | beta * cos(\omega / 2) | $
index_1 = find( abs(beta_cos) <= 1 );
index_2 = find( abs(beta_cos) > 1 );

% inicializamos vector de respuesta en frecuencia de ventana
W = zeros(1, L_dol);

% armamos funcion de respuesta en frecuencia
W(index_1) = cos( (L_dol-1) * acos( beta_cos(index_1) ) );
W(index_2) = cosh( (L_dol-1) * acosh( beta_cos(index_2) ) );

% tomamos parte real, corrigiendo errores numericos (parte imaginaria del orden del error numerico)
W = real(W);

% consideramos caso particular: L par
if ( rem(L_dol, 2) == 0) % (consultamos resto de division: L/2)
	W = W .* exp( (1i*pi/L_dol) * (0:L_dol-1) );
end

% 1. anti-transformamos (IFFT, implementacion de IDFT)
% 2. tomamos parte real, corrigiendo errores numericos (parte imaginaria del orden del error numerico)
% 3. intercambiamos (fftshift) las "mitades" de la señal obtenida (FFT opera circularmente)
w = fftshift( real( ifft(W) ) );

% escalamos ventana, tal que el coeficiente central valga: 1
C =  1 / w( floor( (L_dol+1)/2 ) );
w_dolph = C * w;

wvtool(w_dolph); % vwtool() (matlab) grafica la ventana y su rta. en frecuencia (modulo)

% NOTAR: en grafico anterior, los puntos extremos de w[n]... vamos de nuevo graficando sin interpolar
%figure;
%plot(w_dolph, 'x');

% comparamos con funcion chebwin() provista por matlab
%w_dolph_matlab = chebwin(L_dol, (-1)*alpha); 	% entrega ventana como vector columna

% NOTAR: orden de magnitud
%figure;
%plot( w_dolph - w_dolph_matlab', 'x');

[h_ideal, h_ideal_lp, h_ideal_hp] = utils.h_stop_band(L_dol-1);

% Ventaneamos
h = h_ideal .* w_dolph;

% Comparamos
%h_dolph_matlab = fir1( N_dol, [ mean( omega_wc(1:2)) mean( omega_wc(3:4)) ], 'bandpass', w_dolph_matlab );
%figure;
%plot( h_dolph - h_dolph_matlab, 'x' );

%  % Resp. en frecuencia (modulo), desprolijamente
%  H = fft(h_dolph, 1024);
%  fn = [0 : 1/numel(H) : 1-1/numel(H) ]*2*pi; % intervalo [0,2*pi]
%  figure;
%  plot(fn(1:end/2)/pi, 20*log10(abs(H(1:end/2))), '--xr'); % frec. normalizada

% Respuesta en frecuencia, modulo [dB] y fase
figure;
freqz(h);

%% Aplicando el filtro

y_dolph = utils.filtrar(x,h,'dolph');


%% Análisis de H

[delta_p_obtenido, delta_s_obtenido] = utils.deltas(h);

delta_p - delta_p_obtenido
delta_s - delta_s_obtenido

[SNR_X, E_X] = utils.SNR(x);
[SNR_Y, E_Y] = utils.SNR(y_dolph);

E_Y / E_X
SNR_Y - SNR_X
