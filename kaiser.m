tp1;

%% Kaiser

A = 60; % (7.61) A = -20 * log10(delta_s)
beta = 0.1102*(A-8.7); %(7.62)

M = (A-8) / (2.285 * delta_omega * pi); % (7.63)
M = ceil(M);
if mod(M, 2) == 1 % Buscamos un filtro de Tipo I, el orden *debe* ser PAR
    M = M+1
end
% M = 172
alpha = M/2;
n = 0:M;
k = beta*(1-((n-alpha)/alpha).^2).^0.5;
w = besseli(0,k) / besseli(0,beta);

[h_ideal, h_idel_lp, h_ideal_hp] = utils.h_stop_band(M);

h = h_ideal.*w;

%% Aplicando el filtro

y_kaiser = utils.filtrar(x,h,'kaiser');

%% Análisis de H

[delta_p_obtenido, delta_s_obtenido] = utils.deltas(h);

delta_p - delta_p_obtenido
delta_s - delta_s_obtenido

[SNR_X, E_X] = utils.SNR(x);
[SNR_Y, E_Y] = utils.SNR(y_kaiser);

E_Y / E_X
SNR_Y - SNR_X
