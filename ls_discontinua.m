% Diseño utilizando método LS, FIR Tipo I

tp1;

N = 162; % Orden del filtro, *debe* ser PAR
K = N/2;

n_f = 16*N; % cantidad de puntos en el intervalo 0, pi

index = 1:n_f;
omega = index/n_f * pi;
n = 0:K;


tb_length = floor(delta_omega*n_f); % Transition band
sb_length = ceil((omega_s2-omega_s1)*n_f); % stop band
pb_b_length = ceil((1-omega_p2)*n_f); % second pass band
%pb_a_length = ceil(omega_p1*n_f); % first Pass band
pb_a_length = n_f - (2 * tb_length + sb_length + pb_b_length);

A_d = [ones(ceil(pb_a_length+tb_length/2),1);
       zeros(sb_length+tb_length,1);
       ones(floor(pb_b_length+tb_length/2),1)];

W = [ 1/delta_p * ones(pb_a_length, 1);
              1 * ones(tb_length,1);
      1/delta_s * ones(sb_length, 1);
              1 * ones(tb_length, 1);
      1/delta_p * ones(pb_b_length, 1)];
 

C = cos(repmat(omega',1,K+1) .* repmat(n,n_f,1)) .* repmat(W,1,K+1);
b = W .* A_d;
g = C \ b;


% Armo h (5.150b y 5.150c)
g_aux = g(2:length(g))/2;
    
h = [flipud(g_aux)', g(1), g_aux'];

figure;
freqz(h);

%% Aplicando el filtro

y_ls = utils.filtrar(x,h,'ls');

%% Análisis de H

[delta_p_obtenido, delta_s_obtenido] = utils.deltas(h);

delta_p - delta_p_obtenido
delta_s - delta_s_obtenido

[SNR_X, E_X] = utils.SNR(x);
[SNR_Y, E_Y] = utils.SNR(y_ls);

E_Y / E_X
SNR_Y - SNR_X