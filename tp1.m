% [6638] Procesamiento de Señales I
% Depto. de Electronica - FIUBA
% Diego Mascialino - dmascialino at gmail dot com
%
% (2014.05.03) Trabajo Práctico 1, usando ventana de Kaiser.
close all; clear all; clc; echo on;
utils=utils;

% Aquí se establecen las variables compartidas por los 3 filtros.
global NFFT FS_2_index omega_p1 omega_s1 omega_s2 omega_p2;
global omega_s1_index omega_s2_index omega_p1_index omega_p2_index;
global delta_omega Fs t f omega_normalizado;

% Quiero analizar qué pasa en 0.7425 y 0.7575, necesito (NFFT/2)-1 <= 1e-4
NFFT = 2^15;

FS_2_index = NFFT/2+1;

%% Requerimientos del filtro digital
% en terminos de parámetros de respuesta en frecuencia en el dominio discreto. 

% FIR con fase lineal, de caracteristica "band stop", con los siguientes 
%  parámetros.
% $2 \frac{\delta_w}{2} + 60 Hz \le \frac{4 Khz}{10}
% => \delta_w \le 170 Hz
omega_p1 = .75 - .1/2;    % (R7) (f_ruido - 200)/ (Fs/2)
omega_s1 = .75 - .75/100; % (R6) (f_ruido - 30) / (Fs/2)
omega_s2 = .75 + .75/100; % (R6) (f_ruido + 30) / (Fs/2)
omega_p2 = .75 + .1/2;    % (R7) (f_ruido + 200)/ (Fs/2)
delta_p = 0.01; % (R3)
delta_s = 0.001; % R5

delta_omega = omega_s1 - omega_p1;

omega_s1_index = floor(omega_s1*NFFT/2)+1;
omega_s2_index = ceil(omega_s2*NFFT/2)+1;
omega_p1_index = ceil(omega_p1*NFFT/2)+1;
omega_p2_index = floor(omega_p2*NFFT/2)+1;


global utils;
utils=utils;


%% Análisis señal de entrada

% Leemos la señal a filtrar
[x, Fs] = audioread('audio_con_ruido.wav');
Ts = 1/Fs;
LENGTH_X = length(x); % cantidad de muestras del audio
t = Ts * (0:LENGTH_X);
omega_normalizado = linspace(0,1,FS_2_index);
f = Fs/2*omega_normalizado;

% Gráficos de la señal de entrada
% figure()
% subplot(2,1,1);
% plot(t(1:1000), x(1:1000));
% title('Amplitude of x(t)');
% xlabel('Time (seconds)');
% ylabel('input(t)');
% 
% subplot(2,1,2);
% X = fft(x) / LENGTH_X;
% semilogy(f,2*abs(X(1:length(f))));
% title('Single-Sided Amplitude Spectrum of x(t)');
% xlabel('Frequency (Hz)');
% ylabel('|X(f)|');