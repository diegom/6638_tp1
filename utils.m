function funs = utils
  funs.SNR=@SNR;
  funs.h_stop_band=@h_stop_band;
  funs.deltas=@deltas;
  funs.filtrar=@filtrar;
end

function [h,h_ideal_hp, h_ideal_lp]=h_stop_band(N)
global omega_s1 omega_s2 delta_omega;

n_N2 = (0:N) - N/2;

omega_bajo = (omega_s1 - delta_omega / 2) * pi;
omega_alto = (omega_s2 + delta_omega / 2) * pi;

h_ideal_lp = omega_bajo/pi * sinc(omega_bajo/pi * n_N2);

h_ideal_hp = (pi-omega_alto)/pi * sinc((pi-omega_alto)/pi * n_N2) .* (-1).^n_N2;

h = h_ideal_lp + h_ideal_hp;
end

function [delta_p_obtenido, delta_s_obtenido]=deltas(h)
global NFFT omega_p1_index omega_s1_index omega_s2_index omega_p2_index FS_2_index omega_normalizado;
H = fft(h,NFFT);
H = abs(H(1:FS_2_index));
H = H.';

delta_p_obtenido = max(vertcat(H(1:omega_p1_index), H(omega_p2_index:FS_2_index))-1);
delta_s_obtenido = max(H(omega_s1_index:omega_s2_index));
figure;
plot(omega_normalizado, 20*log10(H));
xlabel('f / (Fs/2)');
ylabel('|H| [dB]');
end

function [SNR, S_X]=SNR(x)
global omega_s1 omega_s2;

X = fft(x);
NFFT = length(x);
FS_2_index = NFFT/2+1;

omega_s1_index = floor(omega_s1*NFFT/2)+1;
omega_s2_index = ceil(omega_s2*NFFT/2)+1;

EDS_X = 1/(2*pi)*vertcat(abs(X(1)), 2*abs(X(2:FS_2_index))).^2; % Parseval, (2.167)

S_X = sum(vertcat(EDS_X(1:omega_s1_index), EDS_X(omega_s2_index:FS_2_index)));
N_X = sum(EDS_X(omega_s1_index:omega_s2_index));

SNR = 20 * log10(S_X/N_X);
end

function y=filtrar(x,h, nombre)
global Fs t f NFFT;
y = conv(x,h);
audiowrite(strcat(nombre,'.wav'), y, Fs);

% Gráficos de la señal de salida
figure()
%subplot(2,1,1);
plot(t(1:2500), y(1:2500));
title(strcat('Amplitude of y(t), window: ', nombre));
xlabel('Time (seconds)');
ylabel('input(t)');
% 
% subplot(2,1,2);
% Y = fft(y);
% semilogy(f,2*abs(Y(1:length(f))));
% title(strcat('Single-Sided Amplitude Spectrum of y(t), window: ', nombre));
% xlabel('Frequency (Hz)');
% ylabel('|Y(f)|');
end